from fabric.api import env, run, sudo, local, settings
from fabric.context_managers import cd, prefix
#from fabric.api import env, run, cd, sudo, put, require, settings, hide, puts, local
from fabric.decorators import roles
from fabric.contrib import project, files
import re

######## CHANGE BELOW TO SUIT YOUR PROJECT #######

env.deploy_user = "walkfit"
env.home_dir = "/home/{0}/Dev/".format(env.deploy_user)

env.local_host = "11.0.0.80"
env.process = 'walkfit_Dev'
env.dbdump_file = 'moodymedia_production-nightly.sql'

######### DO NOT EDIT BELOW THIS LINE ############

def v():
    """Use Vagrant for testing"""
    env.user = 'vagrant'
    env.hosts = [env.local_host]   # Update the Vagrantinit file if you change this

    # retrieve the IdentityFile:
    result = local('vagrant ssh-config | grep IdentityFile', capture=True)
    env.key_filename = re.sub(r'^"|"$', '', result.split()[1])  # parse IdentityFile

def ve(command):
    with cd(env.home_dir + 'moodymedia_org/'):
        sudo("source %svenv/bin/activate" % env.home_dir + " && " + command, user=env.deploy_user)

def collect():
    """
    Run collect static.
    """
    with cd(env.home_dir):
        ve('python manage.py collectstatic --noinput')

def manage(cmd="", extra=""):
    '''
    Run a django management command
    '''
    with cd(env.home_dir):
        ve('python manage.py {0} {1}'.format(cmd, extra))

def reqs(cmd="", extra=""):
    '''
    Re install requirements.txt file
    '''
    with cd(env.home_dir):
        ve('pip install -r requirements.txt')

def log():
    """
    View our logs
    """
    sudo ('tail -n 50 /var/log/upstart/walkfit_Dev.log')

def hup():
    '''
    HUP gunicorn
    '''
    sudo("restart %s" % env.process)

def free():
    """Check free memory."""
    run('free -m')
