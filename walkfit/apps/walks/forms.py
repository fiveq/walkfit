from django.core.urlresolvers import reverse
from django.forms.models import inlineformset_factory 
import floppyforms as forms
from .models import Waypoint, Walk


WaypointFormSet = inlineformset_factory(Walk, Waypoint, extra=2,
    fields=('title','description','address','city','state','zipcode'),
    can_delete=True)


class WalkForm(forms.ModelForm):
    class Meta:
        model = Walk
        exclude = ['last_waypoint', 'participants', 'admins', 'invitations']

    def get_success_url(self):
        return reverse('dashboard')

