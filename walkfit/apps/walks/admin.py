from django.contrib.gis import admin

from .models import (Walk, Checkin, Waypoint, Invitation,
                     WalkProgress)

class WaypointAdmin(admin.GeoModelAdmin):
    list_display = ('title', 'order')
    list_editable = ('order',)
    default_zoom = 10

admin.site.register(Walk)
admin.site.register(WalkProgress)
admin.site.register(Checkin)
admin.site.register(Invitation)
admin.site.register(Waypoint, WaypointAdmin)
