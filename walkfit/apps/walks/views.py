import json
from django.http import HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.views.generic.edit import CreateView, UpdateView, FormView
from django.views.generic import DetailView, ListView, TemplateView, View
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.measure import D
from django.template.loader import render_to_string
from django.core import serializers
from django.shortcuts import get_object_or_404, redirect
from django.conf import settings
from templated_email import send_templated_mail
from django.contrib.sites.models import get_current_site

from .models import Walk, Waypoint, Checkin, Invitation, WalkProgress
from .forms import WaypointFormSet, WalkForm
from braces import views


class JsonView(views.CsrfExemptMixin,
               views.JsonRequestResponseMixin,
               views.JSONResponseMixin, View):
    pass


class WalkDetailJSONView(JsonView, DetailView):
    model = Walk
    json_dumps_kwargs = {u"indent": 2}

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        context_dict = {
            u"title": self.object.title,
            u"description": self.object.description
        }

        return self.render_json_response(context_dict)


class WalkDetailView(JsonView, DetailView):
    model = Walk

    def get_context_data(self, **kwargs):
        context = super(WalkDetailView, self).get_context_data(**kwargs)
        try:
            context['status'] = WalkProgress.objects.get(user=self.request.user, walk=self.object)
        except:
            context['status'] = None
        return context

class WalkWaypointView(View):

    def form_valid(self, form):
        context = self.get_context_data()
        waypoint_form = context['waypoint_form']
        if waypoint_form.is_valid():
            self.object = form.save()
            waypoint_form.instance = self.object
            waypoint_form.save()
            return HttpResponseRedirect(reverse(object.get_absolute_url))
        else:
            return self.render_to_response(self.get_context_data(form=form))


class WalkCreateView(CreateView, WalkWaypointView):
    model = Walk

    def get_context_data(self, **kwargs):
        context = super(WalkCreateView, self).get_context_data(**kwargs)
        context['test'] = True
        if self.request.POST:
            context['waypoint_form'] = WaypointFormSet(self.request.POST)
        else:
            context['waypoint_form'] = WaypointFormSet()
        return context


class WalkUpdateView(UpdateView, WalkWaypointView):
    model = Walk
    form_class = WalkForm

    def get_context_data(self, **kwargs):
        context = super(WalkUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['waypoint_form'] = WaypointFormSet(self.request.POST)
        else:
            context['waypoint_form'] = WaypointFormSet(instance=context['object'])
        return context


class WalkListJSONView(JsonView, ListView):
    model = Walk
    json_dumps_kwargs = {u"indent": 2}

    def get(self, request, *args, **kwargs):
        context = serializers.serialize('json',
                                        self.get_queryset().all())

        return self.render_json_response(context)


class WalkListView(JsonView, ListView):
    model = Walk
    #form_class = ProjectForm

class WaypointDetailView(JsonView, DetailView):
    model = Waypoint

class WaypointCreateView(CreateView):
    model = Waypoint
    #form_class = WaypointForm


class WaypointListJSONView(JsonView, ListView):
    model = Waypoint
    json_dumps_kwargs = {u"indent": 2}

    def get(self, request, *args, **kwargs):
        context = serializers.serialize('json',
                                        self.get_queryset().all())

        return self.render_json_response(context)


class WaypointListView(JsonView, ListView):
    model = Waypoint
    #form_class = ProjectForm

class DashboardView(TemplateView):
    template_name = 'homepage.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['ESRI_ID'] = getattr(settings, 'LEAFLET_ESRI_ID', None)
        context['ESRI_CODE'] = getattr(settings, 'LEAFLET_ESRI_CODE', None)
        try:
            context['progress'] = WalkProgress.objects.filter(user=self.request.user).latest()
        except:
            pass
        if self.request.user.is_authenticated():
            context['walks'] = self.request.user.participants.all()
        return context
    
    pass

def checkin(request, slug):
    if request.POST:
        try:
            steps = request.POST['steps']
        except:
            steps = None

        walk = get_object_or_404(Walk, slug=slug)
        user = request.user
        try:
            prev = Checkin.objects.filter(user=user, walk=walk).latest()
        except:
            prev = None
        checkin = Checkin.create(walk=walk,
                                 user=user,
                                 steps=steps,
                                 prev=prev)
        checkin.save()
        distance = round((float(steps) / walk.steps_per_mile), 2)

    response_data = '<h2>Check-in Recorded. Great job!!</h2> <p>You contributed {0} miles to your team!'.format(distance)
    return HttpResponse(json.dumps(response_data), 
                        content_type="application/json")


def send_invite(request, slug):
    if request.POST:
        try:
            recipient = request.POST['email']
        except IndexError:
            recipient = None
         
        walk = get_object_or_404(Walk, slug=slug)
        invite = Invitation.objects.create(email=recipient)
        invite.save()
        walk.invitations.add(invite)
        full_url = ''.join(['http://', get_current_site(request).domain, walk.get_absolute_url()])
        send_templated_mail(
            template_name='invite',
            from_email='info@walkfit.io',
            recipient_list=[recipient],
            context={
                'walk': walk,
                'url': full_url,
            },
    )
    response_data = '<h2>Invitation dispatched!</h2>'
    return HttpResponse(json.dumps(response_data), 
                        content_type="application/json")

