from django.db import models
from localflavor.us.models import PhoneNumberField, USStateField
from django.utils.translation import ugettext_lazy as _
from django.db.models import permalink
from django_extensions.db.models import TimeStampedModel
from django_extensions.db.fields import AutoSlugField
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from taggit.managers import TaggableManager
from taggit.models import TaggedItemBase
from ckeditor.fields import RichTextField
from django.contrib.gis.geos import Point, GEOSGeometry
from django.conf import settings
from datetime import datetime
from allauth.account.signals import user_signed_up

from .utils import get_lat_long

class ActiveManager(models.Manager):
    def get_queryset(self):
        return super(ActiveManager, self).get_queryset().filter(
                start_date__gte=datetime.now(),
                end_date__lte=datetime.now(), active=True)


class Invitation(TimeStampedModel):
    email = models.EmailField(_('email'))

    def __unicode__(self):
        return u'{0}'.format(self.email)


class Walk(TimeStampedModel):
    title = models.CharField(_('title'), max_length=255)
    slug = AutoSlugField(_('slug'), populate_from=['title'])
    description = RichTextField(_('description'))
    public = models.BooleanField(_('public'), default=False)
    start_date = models.DateTimeField(_('start date'), blank=True, null=True)
    end_date = models.DateTimeField(_('end date'), blank=True, null=True)
    active = models.BooleanField(_('active'), default=True)
    steps_per_mile = models.IntegerField(_('steps per mile'), blank=True, null=True)
    last_waypoint = models.ForeignKey('Waypoint', related_name='last_waypoint', blank=True, null=True)
    participants = models.ManyToManyField(get_user_model(), related_name='participants')
    admins = models.ManyToManyField(get_user_model(), related_name='admins')
    invitations = models.ManyToManyField(Invitation, blank=True, null=True)


    def __unicode__(self):
        return u'{0}'.format(self.title)

    @property
    def miles(self):
        return sum([wp.miles if wp.miles is not None else 0 for wp in self.waypoint_set.all()])

    @property
    def steps(self):
        return sum([wp.steps if wp.steps is not None else 0 for wp in self.waypoint_set.all()])

    @property
    def steps(self):
        steps = 0
        for p in self.walkprogress_set.all():
            steps += p.total_steps 
        return steps

    @property
    def miles(self):
        return self.steps / self.steps_per_mile

    @property
    def meters(self):
        return (self.steps / self.steps_per_mile) * 1609.34

    @permalink
    def get_absolute_url(self):
        return ('walk-detail', None, {'slug': self.slug})


class Waypoint(TimeStampedModel):
    walk = models.ForeignKey(Walk)
    order = models.IntegerField(_('order'))
    title = models.CharField(_('title'), max_length=255)
    slug = AutoSlugField(_('slug'), populate_from=['walk', 'title'])
    description = RichTextField(_('description'), blank=True, null=True)
    address = models.CharField(_('address'),
                               max_length=255, blank=True, null=True)
    city = models.CharField(_('city'), max_length=100, blank=True, null=True)
    state = USStateField(_('state'), blank=True, null=True)
    zipcode = models.CharField(_('zip'), max_length=5, blank=True, null=True)
    lat_long = models.CharField(_('starting lat and long coords'),
                                max_length=255, blank=True, null=True)
    point = gis_models.PointField(blank=True, null=True, srid=4326)
    miles = models.FloatField(_('miles'), blank=True, null=True)
    steps = models.IntegerField(_('steps'), blank=True, null=True)

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return u'{0} waypoint - {1}'.format(self.walk, self.title)

    @property
    def js_slug(self):
        return self.slug.replace('-', '_')

    @property
    def previous_point(self):
        try:
            pt = Waypoint.objects.filter(walk=self.walk).get(order=self.order-1)
        except:
            pt = None
        return pt

    @property
    def next_point(self):
        try:
            pt = Waypoint.objects.filter(walk=self.walk).get(order=self.order+1)
        except:
            pt = None
        return pt

    @property
    def latitude(self):
        return float(self.lat_long.split(',')[0])

    @property
    def longitude(self):
        return float(self.lat_long.split(',')[1])

    @permalink
    def get_absolute_url(self):
        return ('waypoint-detail', None, {'slug': self.slug})

    def save(self):
        if self.next_point:
            try:
                self.miles = self.point.distance(self.next_point.point) * 100
                if not self.walk.steps_per_mile:
                    spm = getattr(self, 'WALKS_STEPS_PER_MILES', 2000)
                else:
                    spm = self.walk.steps_per_mile
                self.steps = int(self.miles * spm)
            except ValueError:
                pass

        location = "%s+%s+%s+%s" % (self.address.replace(' ', '+'),
                                    self.city,
                                    self.state,
                                    self.zipcode)
        self.lat_long = get_lat_long(location)
        if not self.lat_long:
            location = "%s+%s+%s" % (self.city, self.state, self.zipcode)
            self.lat_long = get_lat_long(location)
        self.point = Point(self.longitude, self.latitude)
        super(Waypoint, self).save()


class WalkProgress(TimeStampedModel):
    '''
    A per-user tracking of walk progress.
    
    For all-user tracking of walk progress, please
    see the Walk model.
    '''
    walk = models.ForeignKey(Walk)
    user = models.ForeignKey(get_user_model())
    last_waypoint = models.ForeignKey('Waypoint')
    total_steps = models.IntegerField(_('total steps'), default=0)

    class Meta:
        get_latest_by = 'created'

    @property
    def miles(self):
        return (self.total_steps / self.walk.steps_per_mile)

    @property
    def meters(self):
        return self.miles * 1609.34

    def __unicode__(self):
        return u'Progress on {0} for {1}'.format(self.walk, self.user)


class Checkin(TimeStampedModel):
    walk = models.ForeignKey(Walk)
    user = models.ForeignKey(get_user_model())
    steps = models.IntegerField(_('steps'))
    date = models.DateField(_('date'), default=datetime.now())
    previous_checkin = models.ForeignKey('self', blank=True, null=True)
    lat_long = models.CharField(_('lat and long coords'),
                                max_length=255, blank=True, null=True)
    point = gis_models.PointField(blank=True, null=True)

    objects = gis_models.GeoManager()

    class Meta:
        get_latest_by = 'date'

    @classmethod
    def create(cls, walk, user, steps, prev):
        checkin = cls(walk=walk, user=user, steps=steps,
                      previous_checkin=prev)
        return checkin

    @property
    def is_start(self):
        start = False
        if not self.previous_checkin:
            start = True
        return start

    @permalink
    def get_absolute_url(self):
        return ('checkin-detail', None, {})

    def __unicode__(self):
        return u'On {0} checked in {1}'.format(self.created, self.user)

    def save(self):
        '''
        First, if there's no previous check-in, use the walk's starting point.

        Next, let's create or update a path for the user, 
        
        '''
        try:
            # First see if a progress indicator for this user and walk already exists
            progress = WalkProgress.objects.get(walk=self.walk, user=self.user)
            progress.total_steps = int(progress.total_steps) + int(self.steps)
        except WalkProgress.DoesNotExist:
            # If not, grab a new one
            progress = WalkProgress(walk=self.walk,
                               user=self.user,
                               last_waypoint=self.walk.waypoint_set.get(order=1),
                               total_steps=self.steps)

        if progress.total_steps >= progress.last_waypoint.next_point.steps:
            progress.last_waypoint = progress.last_waypoint.next_point

        progress.save()
        if self.walk.steps >= self.walk.last_waypoint.next_point.steps:
            self.walk.last_waypoint = progress.last_waypoint = progress.last_waypoint.next_point

        self.walk.save()
        super(Checkin, self).save()

def check_if_invited(sender, **kwargs):
    invites = Invitation.objects.filter(email=kwargs['user'].email)
    if invites:
        walks = Walk.objects.filter(invitations=invites)
        if walks:
            for w in walks:
                w.participants.add(kwargs['user'])
                w.save()
    for i in invites:
        i.delete()

user_signed_up.connect(check_if_invited)
