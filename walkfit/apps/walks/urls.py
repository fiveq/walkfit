from django.conf.urls import patterns, url
from .views import (WalkDetailJSONView, WalkDetailView,
                    WalkListJSONView, WalkListView,
                    WaypointListJSONView, WaypointListView,
                    WalkCreateView, WaypointCreateView,
                    WalkUpdateView, DashboardView, send_invite, checkin)

# custom views
urlpatterns = patterns(
    '',
    url(r'^walks/(?P<slug>[-\w]+)/waypoints/create/$',
        view=WaypointCreateView.as_view(),
        name="walk-create"),

    url(r'^walks/(?P<slug>[-\w]+)/waypoints.json$',
        view=WaypointListJSONView.as_view(),
        name="walk-detail-json"),

    url(r'^walks/(?P<slug>[-\w]+)/waypoints/$',
        view=WaypointListView.as_view(),
        name="walk-detail"),

    url(r'^walks/(?P<slug>[-\w]+)/check-in/$',
        view=checkin,
        name="walk-checkin"),

    url(r'^walks/(?P<slug>[-\w]+)/invite/$', 
        view=send_invite, 
        name="walk-invite"),

    url(r'^walks/(?P<slug>[-\w]+).json$',
        view=WalkDetailJSONView.as_view(),
        name="walk-detail"),

    url(r'^walks/(?P<slug>[-\w]+)/update/$',
        view=WalkUpdateView.as_view(),
        name="walk-update"),

    url(r'^walks/(?P<slug>[-\w]+)/$',
        view=WalkDetailView.as_view(),
        name="walk-detail"),

    url(r'^walks/new/$',
        view=WalkCreateView.as_view(),
        name="walk-create"),

    url(r'^walks.json$',
        view=WalkListJSONView.as_view(),
        name="walk-list-json"),

    url(r'^walks/$', view=WalkListView.as_view(), name="walk-list"),


    url(r'^$',
        view=DashboardView.as_view(),
        name="dashboard"),
)
