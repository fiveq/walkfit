======
Design
======

WalkFit.io is designed to be a fairly simple way to create walking
challenges for a group. 

At it's most basic, Walkfit provides three things:

----------
Challenges
----------

A challenge is the whole kaboodle. A walk across the United States?
Maybe a quick trip from D.C. to Baltimore? What distance would you like
your group to cover? All you really need is a title and a description
if you'd like one at this point. The checkpoints, including the start and
end are input later.

Additionally, a challenge should have at least a start date, and may need
and date in case we don't want this thing hanging out forever.

You are also able to set your challenge to be either private or public. This
is great just in case you want specific addresses of your participants to
be part of the challenge. Don't publish more than you mean to on the 
interwebz!

-----------
Participant
-----------

A participant is going to check-in at various points during the challenge.
The will be able to login, check their current progress and update the app
with recent miles covered.

----------
Checkpoint
----------

A checkpoint is one stop on your walk. This is where you can trigger certain
things to happen when people reach certain points as well. These must include
an address, an optionally a description, image and a link.
