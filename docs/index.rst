.. WalkFit.io documentation master file, created by
   sphinx-quickstart on Mon Sep  8 18:07:21 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to WalkFit.io's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2

   getting_started
   design



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

